<?php

// 不是所有的 method 都在 layout.xml 有对应的 handler 供前端显示的
// 有些 method 仅仅执行了些操作，不向前端显示
// 有些 method 不返回 html，返回 json，即 API
class Base_PageController_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		// 生产环境中不在 controller 的 action 中输出内容
		// echo "It works.";
		$this->loadLayout(); // 去使用 config.xml <layout> 的设置
		$this->renderLayout();
	}

	public function paramsAction()
	{
		foreach( $this->getRequest()->getParams() as $key=>$value ){
			echo 'param: '. $key . ' ' . 'value: ' . $value . '<br/>';
		}
	}
}