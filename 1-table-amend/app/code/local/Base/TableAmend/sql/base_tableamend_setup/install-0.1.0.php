<?php

$installer = $this;

$installer->startSetup();

// 源的 column 是什么样格式要求，就必须在 sales_flat_order_grid 建个一模一样的 column 以便同步
// 这是在 sales/order_address 找到的
/*
$installer->getConnection()
	->addColumn($installer->getTable('sales/order_grid'), 'postcode', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Postcode');
*/

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'postcode', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 255,
        'nullable'  => true,
        'comment'   => 'Postcode'
    ));


$installer->endSetup();