<?php

$installer = $this;

$installer->startSetup();

// 在 newTable 的这步返回了个 table model
// 余下的 addColumn 都是往这个 table model 上添新列
// 直到最末才用一个 createTable 切实创建 table
// TODO: 在本文件陈列所有的 field 类型，方便以后参考和使用
$table = $installer->getConnection()
					->newTable($installer->getTable('nullor_tablemodel/table1'))
					->addColumn('entity_id', 
						Varien_Db_Ddl_Table::TYPE_INTEGER, null, 
						array(
							'identity' => true,
							'unsinged' => true,
							'nullable' => false,
							'primary'  => true),
						'Entity Id')
					->addColumn('field1',
						Varien_Db_Ddl_Table::TYPE_TEXT, 255,
						array(),
						'Field1')
					->addIndex($installer->getIdxName('nullor_tablemodel/table1',
													array('field1'),
													array('field1'))
					;

$installer->getConnection()->createTable('$table');

$installer->endSetup();